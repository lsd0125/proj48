<?php
require __DIR__ . '/__connect_db.php';
$pname = 'register';
$success = false;

if(isset($_POST['email'])) {
    echo '<!-- <pre>';
    print_r($_POST);
    echo '</pre> -->';

    $hash = md5( $_POST['email']. uniqid() );


    $sql = sprintf("INSERT INTO `members`(`id`, `email`, `nickname`,
 `password`, `mobile`, `address`, 
 `birthday`, `hash`, `created_at`) VALUES (
 NULL, '%s', '%s', 
 '%s', '%s', '%s',
 '%s', '%s', NOW()
  )",
        $mysqli->escape_string($_POST['email']),
        $mysqli->escape_string($_POST['nickname']),

        sha1($_POST['password']),
        $mysqli->escape_string($_POST['mobile']),
        $mysqli->escape_string($_POST['address']),

        $mysqli->escape_string($_POST['birthday']),
        $hash
    );
//    echo $sql;
//    exit;

    $success = $mysqli->query($sql);

    //$mysqli->errno
    //$mysqli->error
    //exit; //die();
}
?>
<?php include  __DIR__. '/__html_head.php'; ?>
    <style>
        .info {
            color: red;
            font-weight: bold;
        }
    </style>
<div class="container">
    <?php include __DIR__ . '/__navbar.php'; ?>

    <?php if($success): ?>
    <div class="col-md-12">
        <div class="alert alert-success" role="alert">
            註冊完成
        </div>
    </div>
    <?php endif; ?>
    <div class="col-md-6">

        <div class="row">
            <div class="panel panel-primary">
                <div class="panel-heading"><h3 class="panel-title">會員註冊</h3></div>
                <div class="panel-body">

                    <form name="form1" method="post" onsubmit="return checkForm();">
                        <div class="form-group">
                            <label for="email">Email ** <span class="info"></span></label>
                            <input type="text" class="form-control" id="email" name="email">
                        </div>
                        <div class="form-group">
                            <label for="nickname">暱稱 ** <span class="info"></span></label>
                            <input type="text" class="form-control" id="nickname" name="nickname">
                        </div>
                        <div class="form-group">
                            <label for="password">密碼 ** <span class="info"></span></label>
                            <input type="password" class="form-control" id="password" name="password">
                        </div>
                        <div class="form-group">
                            <label for="mobile">手機</label>
                            <input type="text" class="form-control" id="mobile" name="mobile">
                        </div>
                        <div class="form-group">
                            <label for="address">地址</label>
                            <input type="text" class="form-control" id="address" name="address">
                        </div>
                        <div class="form-group">
                            <label for="birthday">生日</label>
                            <input type="text" class="form-control" id="birthday" name="birthday">
                        </div>

                        <button type="submit" class="btn btn-default">註冊</button>
                    </form>


                </div>
            </div>

        </div>


    </div>


</div>
    <script>
        function checkForm(){
            var $email = $('#email');
            var $nickname = $('#nickname');
            var $password = $('#password');
            var items = [$email, $nickname, $password];
            var email = $email.val();
            var nickname = $nickname.val();
            var password = $password.val();
            var i;
            var pattern = /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i;
            var isPass = true;

            for(i=0; i<items.length; i++){
                items[i].closest('.form-group').find('.info').text('');
                items[i].css('border-color', '#ccc');
            }



            if(! email){
                $email.closest('.form-group').find('.info').text('請填寫 email !');
                $email.css('border-color', 'red');
                isPass = false;

            }
            if(! pattern.test(email)){
                $email.closest('.form-group').find('.info').text('Email 格式不正確 !');
                $email.css('border-color', 'red');
                isPass = false;

            }
            if(nickname.length < 2){
                $nickname.closest('.form-group').find('.info').text('暱稱至少兩個字 !');
                $nickname.css('border-color', 'red');
                isPass = false;

            }
            if(password.length < 6){
                $password.closest('.form-group').find('.info').text('密碼至少 6 個字元 !');
                $password.css('border-color', 'red');
                isPass = false;

            }

            $.get('aj_check_email.php', {email: email}, function(data){
                if(data=='1'){
                    $email.closest('.form-group').find('.info').text('此 email 已經使用過了!');
                    $email.css('border-color', 'red');
                    isPass = false;
                }

                if(isPass){
                    document.form1.submit();
                }


            });


            return false;

        }

    </script>
<?php include  __DIR__. '/__html_foot.php'; ?>