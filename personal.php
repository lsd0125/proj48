<?php
require __DIR__ . '/__connect_db.php';
$pname = 'personal';

if(! isset($_SESSION['user'])){
    header('Location: ./');
    exit;
}

$sql = "SELECT * FROM `members` WHERE `id`=". intval($_SESSION['user']['id']) ;
$rs = $mysqli->query($sql);
$row = $rs->fetch_assoc();

$nickname = $row['nickname'];
$mobile = $row['mobile'];
$address = $row['address'];
$birthday = $row['birthday'];




if(isset($_POST['password'])) {
    echo '<!-- <pre>';
    print_r($_POST);
    echo '</pre> -->';




    $stmt = $mysqli->prepare("UPDATE `members` SET
        `nickname`=?,
        `mobile`=?,
        `address`=?,
        `birthday`=?
        WHERE `id`=? AND `password`=?");

    $stmt->bind_param('ssssis',
        $_POST['nickname'],
        $_POST['mobile'],
        $_POST['address'],
        $_POST['birthday'],
        $_SESSION['user']['id'],
        sha1($_POST['password'])
    );

    $success = $stmt->execute();
    $affected = $stmt->affected_rows;

    $nickname = $_POST['nickname'];
    $mobile = $_POST['mobile'];
    $address = $_POST['address'];
    $birthday = $_POST['birthday'];

    if($affected==1){
        $_SESSION['user']['nickname'] = $_POST['nickname'];
    }

    //echo "\$affected: $affected";
}





?>
<?php include  __DIR__. '/__html_head.php'; ?>
    <style>
        .info {
            color: red;
            font-weight: bold;
        }
    </style>
<div class="container">
    <?php include __DIR__ . '/__navbar.php'; ?>

    <?php if(isset($affected)): ?>
        <?php if($affected==1): ?>
        <div class="col-md-12">
            <div class="alert alert-success" role="alert">
                個人資料修改完成
            </div>
        </div>
        <?php else: ?>
            <div class="col-md-12">
                <div class="alert alert-danger" role="alert">
                    未完成修改, 可能密碼輸入錯誤
                </div>
            </div>
        <?php endif; ?>
    <?php endif; ?>
    <div class="col-md-6">

        <div class="row">
            <div class="panel panel-primary">
                <div class="panel-heading"><h3 class="panel-title">個人資料修改</h3></div>
                <div class="panel-body">

                    <form name="form1" method="post" onsubmit="return checkForm();">
                        <div class="form-group">
                            <label for="email">Email <span class="info"></span></label>
                            <input type="text" class="form-control" id="email" name="email"
                                   value="<?= $row['email'] ?>" disabled="disabled">
                        </div>
                        <div class="form-group">
                            <label for="nickname">暱稱 ** <span class="info"></span></label>
                            <input type="text" class="form-control" id="nickname" name="nickname"
                                   value="<?= $nickname ?>">
                        </div>

                        <div class="form-group">
                            <label for="mobile">手機</label>
                            <input type="text" class="form-control" id="mobile" name="mobile"
                                   value="<?= $mobile ?>">
                        </div>
                        <div class="form-group">
                            <label for="address">地址</label>
                            <input type="text" class="form-control" id="address" name="address"
                                   value="<?= $address ?>">
                        </div>
                        <div class="form-group">
                            <label for="birthday">生日</label>
                            <input type="text" class="form-control" id="birthday" name="birthday"
                                   value="<?= $birthday ?>">
                        </div>

                        <div class="form-group">
                            <label for="password">密碼 ** <span class="info"></span></label>
                            <input type="password" class="form-control" id="password" name="password">
                        </div>
                        <button type="submit" class="btn btn-default">修改</button>
                    </form>


                </div>
            </div>

        </div>


    </div>


</div>
    <script>
        function checkForm(){
            var $nickname = $('#nickname');
            var $password = $('#password');
            var items = [$nickname, $password];
            var nickname = $nickname.val();
            var password = $password.val();
            var i;
            var isPass = true;

            for(i=0; i<items.length; i++){
                items[i].closest('.form-group').find('.info').text('');
                items[i].css('border-color', '#ccc');
            }

            if(nickname.length < 2){
                $nickname.closest('.form-group').find('.info').text('暱稱至少兩個字 !');
                $nickname.css('border-color', 'red');
                isPass = false;

            }
            if(password.length < 6){
                $password.closest('.form-group').find('.info').text('密碼至少 6 個字元 !');
                $password.css('border-color', 'red');
                isPass = false;

            }


            return isPass;

        }

    </script>
<?php include  __DIR__. '/__html_foot.php'; ?>