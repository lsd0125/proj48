<?php
require __DIR__ . '/__connect_db.php';

if(!isset($_GET['email'])){
    exit;
}


$result = array(
    'success' => true,
    'info' => array()
);

$email = $mysqli->escape_string($_GET['email']);
$rs = $mysqli->query("SELECT 1 FROM `members` WHERE `email`='$email'");

if($rs->num_rows==1){
    $result['success'] = false;

    $result['info']['email'] = array(
        'num_rows' => $rs->num_rows,
        'msg' => '這個 email 已經有人用了',
    );
}else{
    $result['info']['email'] = array(
        'num_rows' => 0,
    );

}

if( strlen($_GET['nickname']) < 2){
    $result['success'] = false;

    $result['info']['nickname'] = array(
        'msg' => '暱稱長度至少二個字元',
    );
} else {
    $result['info']['nickname'] = array(
    );
}

if( strlen($_GET['password']) < 6){
    $result['success'] = false;

    $result['info']['password'] = array(
        'msg' => '密碼長度至少六個字元',
    );
} else {
    $result['info']['password'] = array(
    );
}


echo json_encode($result, JSON_UNESCAPED_UNICODE);







