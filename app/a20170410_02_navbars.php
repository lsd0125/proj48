<?php
require __DIR__. '/__connect_db.php';

$sql = "SELECT * FROM `categories`";

$rs = $mysqli->query($sql);

$c_ar = array();
while($row= $rs->fetch_assoc()) {

    if($row['parent_sid']==0){
        $c_ar[$row['sid']] = $row;
    }
}
$rs->data_seek(0); // result 內部指標移到第一筆
while($row= $rs->fetch_assoc()) {
    if( isset( $c_ar[$row['parent_sid']] ) ){

        //echo $row['parent_sid']. "<br>";

        $parent = &$c_ar[$row['parent_sid']];
        $parent['subs'][] = $row;

        // $c_ar[$row['parent_sid']]['subs'][] = $row;
    }
}

//print_r($c_ar);


?>
<!doctype html>
<html lang="zh">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>

    <link rel="stylesheet" href="../bootstrap/css/bootstrap.css">
    <link rel="stylesheet" href="../bootstrap/css/bootstrap-theme.css">

    <script src="../lib/jquery-3.1.1.js"></script>
    <script src="../bootstrap/js/bootstrap.js"></script>


</head>
<body>
<div class="container">
    <nav class="navbar navbar-default">
        <div class="container-fluid">
            <div class="navbar-header">
                <button type="button" class="collapsed navbar-toggle" data-toggle="collapse"
                        data-target="#bs-example-navbar-collapse-1" aria-expanded="false"><span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span>
                </button>
                <a href="#" class="navbar-brand">Brand</a></div>
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav">
                    <?php foreach($c_ar as $k1=>$v1): ?>
                    <li class="dropdown"><a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button"
                                            aria-haspopup="true" aria-expanded="false"><?= $v1['name'] ?> <span
                                    class="caret"></span></a>

                        <ul class="dropdown-menu">
                            <?php foreach( $v1['subs'] as $k2=>$v2): ?>
                            <li><a href="?sid=<?= $v2['sid'] ?>"><?= $v2['name'] ?></a></li>
                            <?php endforeach; ?>
                        </ul>

                    </li>
                    <?php endforeach;  ?>
                </ul>

            </div>
        </div>
    </nav>

</div>
</body>
</html>