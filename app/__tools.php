<?php

function get_id_by_token($mysqli, $token='')
{
    $result = array(
        'success' => false,
        'code' => 1,
        'msg' => '沒有 token!',
    );

    if(! empty($token)){
        $token = $mysqli->escape_string($token);

        $m_sql = "SELECT * FROM tokens WHERE token='$token'";
        $m_rs = $mysqli->query($m_sql);

        if($m_rs->num_rows>=1){
            $m_row = $m_rs->fetch_assoc();

            if($m_row['active']==1){

                $result['success'] = true;
                $result['msg'] = 'token 有效';
                $result['code'] = 4;
                $result['member_id'] = $m_row['member_sid'];


            } else {
                $result['msg'] = 'token 已失效';
                $result['code'] = 3;
            }


        } else {
            $result['msg'] = '無效的 token';
            $result['code'] = 2;
        }

    }

    return $result;
}


function set_sess ($mysqli, $token='', $key, $value) {
    if(empty($token)){
        return false;
    }

    $sql = "SELECT * FROM sessions WHERE token='$token'";
    $rs = $mysqli->query($sql);

    if($rs->num_rows >0){
        // 已存在資料
        $row = $rs->fetch_assoc();
        $content = json_decode($row['content'], true);

        $content[$key] = $value;

        $u_sql = sprintf("UPDATE sessions 
SET content='%s' 
WHERE token='%s'",
            json_encode($content, JSON_UNESCAPED_UNICODE),
            $token
        );

        $mysqli->query($u_sql);

    } else {
        $content = array(
            $key => $value
        );

        $i_sql = sprintf("INSERT INTO `sessions`(
`sid`, `token`, `content`, `created_at`
) VALUES (
NULL, '%s', '%s', NOW()
)",
            $token,
            json_encode($content, JSON_UNESCAPED_UNICODE)
            );
    }





}

function get_sess ($mysqli, $token='', $key, $value) {




}



