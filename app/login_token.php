<?php
require __DIR__. '/__connect_db.php';

$result = array(
    'success' => false,
    'code' => 1,
    'msg' => '沒有傳入帳密!',
);

if(isset($_POST['email']) and isset($_POST['password'])){
    $sql = sprintf("SELECT * 
FROM `members` 
WHERE `email`='%s' AND `password`='%s'",
        $mysqli->escape_string($_POST['email']),
        sha1($_POST['password'])
        );
    $rs = $mysqli->query($sql);
    if($rs->num_rows>0){
        $result['code'] = 2;
        $result['success'] = true;
        $result['msg'] = '成功登入！';

        $row = $rs->fetch_assoc();

        // 是否要執行單一時間單一裝置登入
        // 若是，舊的 tokens 應該要讓它們失效

        $token = sha1( $row['email']. uniqid() );

        $insert_sql = "INSERT INTO `tokens`(
`sid`, `member_sid`, `created_at`, `token`
) VALUES (
  NULL, {$row['id']}, NOW(), '$token'
)";
        $mysqli->query($insert_sql);

        $result['token'] = $token;

    } else {
        $result['msg'] = '帳號或密碼錯誤！';
        $result['code'] = 3;
    }
}

echo json_encode($result, JSON_UNESCAPED_UNICODE);









