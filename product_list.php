<?php
require __DIR__ . '/__connect_db.php';
//$title = '佳佳的牛排店';
$pname = 'product_list';

$per_page = 4;

$cate = isset($_GET['cate']) ? intval($_GET['cate']) : 0;
$page = isset($_GET['page']) ? intval($_GET['page']) : 1;
$search = isset($_GET['search']) ? $_GET['search'] : '';


$c_sql = "SELECT * FROM `categories` WHERE `parent_sid`=0";
$c_rs = $mysqli->query($c_sql);

$where = " WHERE 1 ";
if ($cate) {
    $where .= " AND `category_sid`=$cate ";
}
if($search) {
    $search2 = $mysqli->escape_string( "%{$search}%" );
    $where .= sprintf(" AND (`author` LIKE '%s' OR `bookname` LIKE '%s' ) ", $search2, $search2);
}

// 總共有幾筆
$total_sql = "SELECT COUNT(1) FROM `products` $where ";
$total_rs = $mysqli->query($total_sql);
$total_num = $total_rs->fetch_row()[0];
// 總共有幾頁
$total_pages = ceil($total_num / $per_page);


$p_sql = sprintf("SELECT * FROM `products` %s LIMIT %s, %s", $where, ($page - 1) * $per_page, $per_page);

//echo $p_sql;
//exit;
$p_rs = $mysqli->query($p_sql);


?>
<?php include __DIR__ . '/__html_head.php'; ?>

    <div class="container">
        <?php include __DIR__ . '/__navbar.php'; ?>
.
        <div class="col-md-12">
            <div id="my_info" class="alert alert-success" role="alert" style="display:none"></div>
        </div>
        <div class="row">
            <div class="col-md-3">
                <div class="btn-group-vertical" role="group" aria-label="Vertical button group">
                    <a type="button" class="btn btn-default" href="product_list.php">所有商品</a>
                    <?php while ($row = $c_rs->fetch_assoc()): ?>
                        <a type="button" class="btn btn-default" href="?cate=<?= $row['sid'] ?>">
                            <?= $row['name'] ?></a>
                    <?php endwhile; ?>
                </div>
            </div>
            <div class="col-md-9">
                <div class="col-md-12">
                    <nav>
                        <ul class="pagination pagination-lg">
                            <li><a href="#" aria-label="Previous"><span aria-hidden="true">«</span></a></li>
                            <?php for($i=1; $i<=$total_pages; $i++):
                                $ar = array('page'=>$i);
                                if($cate){
                                    $ar['cate'] = $cate;
                                }
                                if($search){
                                    $ar['search'] = $search;
                                }
                                ?>
                            <li class="<?= $page==$i ? 'active' : '' ?>"><a href="?<?= http_build_query($ar) ?>"><?= $i ?></a></li>
                            <?php endfor ?>
                            <li><a href="#" aria-label="Next"><span aria-hidden="true">»</span></a></li>
                        </ul>
                    </nav>
                </div>
                <div class="col-md-12">
                    <form class="navbar-form navbar-left" method="get">
                        <div class="form-group">
                            <input type="text" class="form-control" name="search" placeholder="Search"
                            value="<?= empty($search) ? '' : $search ?>">
                        </div>
                        <button type="submit" class="btn btn-default">搜尋</button>
                    </form>
                </div>
                <?php while ($row = $p_rs->fetch_assoc()): ?>
                    <div class="col-md-3">
                        <div class="thumbnail" style="height:280px; margin:10px 0;">
                            <a class="single_product" href="single-product.php?sid=1">
                                <img src="imgs/small/<?= $row['book_id'] ?>.jpg" style="width: 100px; height: 135px;">
                            </a>
                            <div class="caption">
                                <h5><?= $row['bookname'] ?></h5>
                                <h5><?= $row['author'] ?></h5>
                                <p>
                                    <span class="glyphicon glyphicon-search"></span>
                                    <span class="label label-info">$ <?= $row['price'] ?></span>
                                    <select name="qty" class="qty">
                                        <option value="1">1</option>
                                        <option value="2">2</option>
                                        <option value="3">3</option>
                                        <option value="4">4</option>
                                        <option value="5">5</option>
                                        <option value="6">6</option>
                                        <option value="7">7</option>
                                        <option value="8">8</option>
                                        <option value="9">9</option>
                                    </select>
                                    <button class="btn btn-warning btn-sm buy_btn" data-sid="<?= $row['sid'] ?>">買</button>
                                </p>
                            </div>
                        </div>
                    </div>
                <?php endwhile; ?>


            </div>


        </div>


    </div>
    <script>
        var isLogin = <?= isset($_SESSION['user']) ? '1' : '0'  ?>;
        var my_info = $('#my_info');
        var show_info = function(info){
            my_info.text(info);
            my_info.slideDown();
            setTimeout(function(){
                my_info.slideUp();
            }, 2000);
        };


        $(function(){
            $('.buy_btn').click(function(){
                /*
                if(! isLogin){
                    alert('請先登入');
                    return;
                }
*/

                var sid = $(this).attr('data-sid');
                var qty = $(this).closest('.caption').find('.qty').val();
                var bookname = $(this).closest('.caption').find('h5').eq(0).text();

                $.get('add_to_cart.php', {sid:sid, qty:qty}, function(data){
                    //alert('商品已加入購物車');
                    show_info(bookname + ' 已加入購物車');

                    calc_items(data);
                }, 'json');

            });
        });





    </script>
<?php include __DIR__ . '/__html_foot.php'; ?>