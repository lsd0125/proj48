<?php
require __DIR__ . '/__connect_db.php';
$pname = 'login';

if(isset($_SESSION['user'])){
    header('Location: ./');
    exit;
}


if(isset($_POST['email'])) {
    echo '<!-- <pre>';
    print_r($_POST);
    echo '</pre> -->';

    $sql = sprintf("SELECT * FROM `members` WHERE `email`='%s' AND `password`='%s'",
        $mysqli->escape_string($_POST['email']),
        sha1($_POST['password'])
    );
//    echo $sql;
//    exit;

    $rs = $mysqli->query($sql);
    if($rs->num_rows) {
        $row = $rs->fetch_assoc();

        $_SESSION['user'] = $row;
    }
}
?>
<?php include  __DIR__. '/__html_head.php'; ?>
    <style>
        .info {
            color: red;
            font-weight: bold;
        }
    </style>
<div class="container">
    <?php include __DIR__ . '/__navbar.php'; ?>

    <?php if(isset($rs)): ?>
        <?php if($rs->num_rows): ?>
        <div class="col-md-12">
            <div class="alert alert-success" role="alert">
                登入成功
            </div>
        </div>
        <?php else: ?>
            <div class="col-md-12">
                <div class="alert alert-danger" role="alert">
                    帳號或密碼錯誤
                </div>
            </div>
        <?php endif; ?>
    <?php endif; ?>
    <?php
//    echo isset($rs). '<br>';
//    echo $rs->num_rows. '<br>';
    ?>

    <?php //if(! isset($rs) or $rs->num_rows==0): ?>
    <?php if(!(isset($rs) and $rs->num_rows)): ?>
        <div class="col-md-6">

        <div class="row">
            <div class="panel panel-primary">
                <div class="panel-heading"><h3 class="panel-title">會員登入</h3></div>
                <div class="panel-body">

                    <form name="form1" method="post" onsubmit="return checkForm();">
                        <div class="form-group">
                            <label for="email">Email ** <span class="info"></span></label>
                            <input type="text" class="form-control" id="email" name="email">
                        </div>

                        <div class="form-group">
                            <label for="password">密碼 ** <span class="info"></span></label>
                            <input type="password" class="form-control" id="password" name="password">
                        </div>

                        <button type="submit" class="btn btn-default">登入</button>
                    </form>


                </div>
            </div>

        </div>

    </div>
    <?php endif ?>

</div>
    <script>
        function checkForm(){
            var $email = $('#email');
            var $password = $('#password');
            var items = [$email, $password];
            var email = $email.val();
            var password = $password.val();
            var i;
            var pattern = /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i;
            var isPass = true;

            for(i=0; i<items.length; i++){
                items[i].closest('.form-group').find('.info').text('');
                items[i].css('border-color', '#ccc');
            }



            if(! email){
                $email.closest('.form-group').find('.info').text('請填寫 email !');
                $email.css('border-color', 'red');
                isPass = false;

            }
            if(! pattern.test(email)){
                $email.closest('.form-group').find('.info').text('Email 格式不正確 !');
                $email.css('border-color', 'red');
                isPass = false;

            }

            if(password.length < 6){
                $password.closest('.form-group').find('.info').text('密碼至少 6 個字元 !');
                $password.css('border-color', 'red');
                isPass = false;

            }

            return isPass;

        }

    </script>
<?php include  __DIR__. '/__html_foot.php'; ?>