<?php
require __DIR__ . '/__connect_db.php';

$pname = 'buy';

if(!empty($_SESSION['cart']) and !empty($_SESSION['user'])) {
    $total = 0;

    $sids = array_keys($_SESSION['cart']);
    $sql = sprintf("SELECT * FROM `products` WHERE `sid` IN (%s)", implode(',', $sids));

    $rs = $mysqli->query($sql);
    $cart_data = array();
    while ($row = $rs->fetch_assoc()) {
        $row['qty'] = $_SESSION['cart'][$row['sid']];

        $total += $row['qty']*$row['price']; //計算總價

        $cart_data[$row['sid']] = $row;
    }

    // 寫入訂單
    $o_sql = sprintf("INSERT INTO `orders`(
      `sid`, `member_sid`, `amount`, `order_date`
      ) VALUES (
      NULL, %s, %s, NOW()
      )",
        intval($_SESSION['user']['id']),
        $total
    );
    $mysqli->query($o_sql);

    // echo $mysqli->insert_id;
    $order_sid = $mysqli->insert_id;

    // 寫入訂單明細

    foreach ($sids as $sid):

        $p_sql = sprintf("INSERT INTO `order_details`(
          `sid`, `order_sid`, `product_sid`, `price`, `quantity`
          ) VALUES (
          NULL, %s, %s, %s, %s
          )",
            $order_sid,
            $sid,
            $cart_data[$sid]['price'],
            $cart_data[$sid]['qty']
            );

        $mysqli->query($p_sql);

    endforeach;


    //INSERT INTO `order_details`(`sid`, `order_sid`, `product_sid`, `price`, `quantity`) VALUES (

    unset($_SESSION['cart']);

    echo 'ok';



}