<nav class="navbar navbar-default">
    <div class="container-fluid">
        <div class="navbar-header">
            <button type="button" class="collapsed navbar-toggle" data-toggle="collapse"
                    data-target="#bs-example-navbar-collapse-1" aria-expanded="false"><span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span>
            </button>
            <a href="#" class="navbar-brand">Brand</a></div>
        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <ul class="nav navbar-nav">
                <li class="<?= $pname=='product_list' ? 'active' : '' ?>"><a href="product_list.php">商品列表</a></li>
                <li class="<?= $pname=='cart_list' ? 'active' : '' ?>"><a href="cart_list.php">購物車
                        <span class="badge cart_count" style="display: none;">0</span></a></li>
            </ul>
            <ul class="nav navbar-nav navbar-right">
                <?php if(isset($_SESSION['user'])): ?>
                    <li class="<?= $pname=='personal' ? 'active' : '' ?>"><a href="personal.php"><?= $_SESSION['user']['nickname'] ?></a></li>
                    <li><a href="logout.php">登出</a></li>

                <?php else: ?>
                    <li class="<?= $pname=='login' ? 'active' : '' ?>"><a href="login.php">登入</a></li>
                    <li class="<?= $pname=='register' ? 'active' : '' ?>"><a href="register.php">會員註冊</a></li>
                <?php endif ?>

            </ul>
        </div>
    </div>
</nav>
<script>
    var cart_count = $('.cart_count');
    function calc_items(obj){
        var total = 0;

        for(var s in obj){
            total += obj[s];
        }
        cart_count.text(total);
    }

    $(function(){
        $.get('add_to_cart.php', function(data){
            calc_items(data);
            cart_count.fadeIn();
        }, 'json');
    });
</script>